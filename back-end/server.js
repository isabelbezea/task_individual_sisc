const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS sisc_abonament(nume VARCHAR(40),prenume VARCHAR(40), varsta VARCHAR(3), cnp VARCHAR(20), telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,  CONSTRAINT pk_cnp PRIMARY KEY(cnp), sex VARCHAR(3))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta=req.body.varsta;
    let cnp=req.body.cnp;
    let sex=req.body.sex;
    let data=req.body.data;
    let birthday=req.body.birthday;
    let error = []

    

    if(!nume || !prenume || !telefon || !email || !facebook || !tipAbonament || !nrCard || !cvv || !varsta || !cnp )
    {
        console.log("Unul sau mai multe campuri nu au fost introduse");
        error.push("Unul sau mai multe campuri nu au fost introduse");
    }
    else 
    {   //validare pentru nume: dimensiune de minim 2, maxim 20 caractere
        if(nume.lenght <2 || nume.lenght >20)
        {
            console.log("Nume invalid");
            error.push("Nume invalid");
        }
        //validare pentru nume: camp ce contine doar litere
        else if(!nume.match("^[A-Za-z]+$"))
        {
            console.log("Numele introdus trebuie sa contina doar litere");
            error.push("Numele introdus trebuie sa contina doar litere");
        }
        //validare pentru prenume: dimensiune de minim 2, maxim 20 caractere
        if(prenume.lenght <2 || prenume.lenght > 20)
        {
            console.log("Prenume invalid");
            error.push("Prenume invalid");
        }
        //validare pentru prenume: camp ce contin doar litere
        else if(!prenume.match("^[A-Za-z]+$"))
        {
            console.log("Prenumele introdus trebuie sa contina doar litere");
            error.push("Prenumele introdus trebuie sa contina doar litere");
        }

        //validare pentru numarul de telefon: lungimea lui sa fie de 10
        if (!telefon.length == 10) 
           {
            console.log("Numarul de telefon trebuie sa fie de 10 cifre");
            error.push("Numarul de telefon trebuie sa fie de 10 cifre");
          }
          //validare pentru numarul de telefon: camp ce contine doar cifre
          else if (!telefon.match("^[0-9]+$")) 
           { console.log("Numarul de telefon trebuie sa contina doar cifre");
             error.push("Numarul de telefon trebuie sa contina doar cifre");
          }
        
          //validare pentru email
          if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) 
           { console.log("Email invalid");
             error.push("Email invalid!");
          }

          //validare link Facebook 
          if(!facebook.includes("facebook.com/"))
         {
             console.log("Adresa de Facebook invalida");
             error.push("Adresa de Facebook invalida");
         }

         //validare pentru numar card: lungime=16;
         if(!nrCard.lenght == 16)
         {
             console.log("Numarul de card trebuie sa contina doar 16 caractere");
             error.push("Numarul de card trebuie sa contina doar 16 caractere");
         }
         //validare pentru numar card: camp ce contine doar cifre
         else if (!nrCard.match("^[0-9]+$")) 
           { console.log("Numarul de card trebuie sa contina doar cifre");
             error.push("Numarul de card trebuie sa contina doar cifre");
          }
           //validare pentru cvv: lungime=3;
         if(!cvv.lenght == 3)
         {
             console.log("CVV-ul trebuie sa contina doar 3 caractere");
             error.push("CVV-ul trebuie sa contina doar 3 caractere");
         }
         //validare pentru CVV: camp ce contine doar cifre
         else if (!nrCard.match("^[0-9]+$")) 
           { console.log("CVV-ul trebuie sa contina doar cifre");
             error.push("CVV-ul trebuie sa contina doar cifre");
          }
          //validare pentru varsta: minim 1, maxim 3 caractere
          if(varsta.lenght <1 || varsta.lenght >3)
         {
             console.log("Varsta este introdusa gresit");
             error.push("Varsta este introdusa gresit");
         }
         //validare pentru varsta: camp ce contine doar cifre
         else if (!nrCard.match("^[0-9]+$")) 
           { console.log("Varsta trebuie sa contina doar cifre");
             error.push("Varsta trebuie sa contina doar cifre");
          }
        //validare pentru cnp: lungime=13;
          if(!cnp.lenght ==13)
         {
             console.log("CNP-ul este introdus gresit");
             error.push("CNP-ul este introdus gresit");
         }
         //validare pentru cnp: camp ce contine doar cifre
         else if (!nrCard.match("^[0-9]+$")) 
           { console.log("CNP-ul trebuie sa contina doar cifre");
             error.push("CNP-ul trebuie sa contina doar cifre");
          }
          
          
          if(cnp.charAt(0) == 1 || cnp.charAt(0) == 3 || cnp.charAt(0) ==5)
             sex= "M";
             else if(cnp.charAt(0) == 2 || cnp.charAt(0) == 4 || cnp.charAt(0) ==6)
                sex= "F";

            //extragem din cnp anul
            const an= cnp.slice(1,3);
            //extragem din cnp luna
            const luna = cnp.slice(3,5);
            //extragem din cnp ziua
            const ziua = cnp.slice(5,7);
            
            const ziua_buna = parseInt(ziua) + 1;
            //formam data conform a ce am extras din cnp
            const data = an + "/" + luna + "/" + ziua_buna; 
            //convertim la tip date
            birthday=new Date(data);
            //functie pentru calcularea varstei pe baza datei de nastere
            function getAge(birthday) {
                var today = new Date();
                var thisYear = 0;
                if (today.getMonth() < birthday.getMonth()) {
                    thisYear = 1;
                } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
                    thisYear = 1;
                }
                var age = today.getFullYear() - birthday.getFullYear() - thisYear;
                
                return age;
            }
            //apelam functia
            const varsta1 = getAge(birthday);
            
            //daca varsta calculata pe baza cnp-ului nu coincide cu varsta introdusa de utilizator, inscrierea nu poate fi   efectuata
            if(varsta1 !=varsta)
            {
                console.log("Varsta din cnp nu coincide cu varsta introdusa");
                error.push("Varsta din cnp nu coincide cu varsta introdusa");
            }



    }



    if (error.length === 0) {
        const sql =
            "INSERT INTO sisc_abonament (nume,prenume,varsta,cnp,telefon,email,facebook,tipAbonament,nrCard,cvv, sex) VALUES('" +nume +"','" +prenume +"','" +varsta +"', '"+cnp+"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"', '" +sex+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});