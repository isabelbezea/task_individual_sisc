$(document).ready(function () {
    $('select').material_select();
});

document.querySelector('#regbutton').addEventListener("click", (e) => {
    e.preventDefault();
    toastr.remove()
    const abonament = {
        nume: document.querySelector('#nume').value,
        prenume: document.querySelector('#prenume').value,
        varsta: document.querySelector("#varsta").value,
        cnp: document.querySelector("#cnp").value,
        telefon: document.querySelector('#telefon').value,
        email: document.querySelector('#email').value,
        facebook: document.querySelector('#facebook').value,
        tipAbonament: document.querySelector('#tipAbonament').value,
        nrCard: document.querySelector('#nrCard').value,
        cvv: document.querySelector("#cvv").value,
       
       
        
        
    }
    axios.post('/abonament', abonament)
        .then((response) => {
            toastr.success("Abonament achizitionat!");
        })
        .catch((error) => {
            const values = Object.values(error.response.data)
            console.log(error);
            values.map(item => {
                toastr.error(item)
            })
        })

}, false)